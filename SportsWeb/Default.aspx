﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SportsWeb._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <div class="row"></div>
        <div class="col-md-6">            
           <h2>Sports Web Proof of Concept</h2>
            <p class="lead">Welcome to the Sports Web.</p> 
        </div>
        <div class="col-md-6">
            <p>
                <img src="https://s3.eu-west-2.amazonaws.com/sportswebs3/SportsWebLogo.png" height="100" width ="400" align="right" /> <%-- class="text-right"/>--%>
            </p>
        </div>
        <div class="row"></div>    
        <div class="row" >        
             <div class="col-md-12">
                 <div class="text-left">
                 <br/>                      
                    <p class="text-left" >     
                    This proof of concept demonstrates a website using polyglot persistence hosted on the AWS cloud. It uses
                    RDS MySQL, Amazon DynamoDB and S3 storage to store different categories of data as explained in the accompanying reports.
                    </p>  
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <h2>Customers</h2>
            <p>
                Customer details are stored on an AWS Relational Database Service (RDS) MySQL Instance dues to the need for strong data consistency. 
            </p>
            <p>             
                <a class="btn btn-default" href="/Customers">Open &raquo;</a>
            </p>
        </div>
        <div class="col-md-6">
            <h2>Sports Web News</h2>
            <p>
                Corporate news stories are dynamic content stored in an Amazon DynamoDB NoSQL table where high data availability is preferred over strong data consistency.
            </p>
            <p>                
                <a class="btn btn-default" href="/News">Open &raquo;</a>
            </p>
        </div>
    </div>
    
</asp:Content>
