﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Odbc;

using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using SportsWebDynamoDBDataModel;

namespace SportsWeb
{
    public partial class News : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                Response.Write("<div class=\"row\">");
                        Response.Write("<div class=\"col-md-6\">");
                            Response.Write("<h1>SportsWeb News</h1>");
                        Response.Write("</div>");
                        Response.Write("<div class=\"col-md-6\">");
                            Response.Write("<img src = \"https://s3.eu-west-2.amazonaws.com/sportswebs3/SportsWebLogo.png\" height=\"100\" width =\"300\" align=\"right\"/>");
                        Response.Write("</div>");
                Response.Write("</div>");
                
                // Iterate through Amazon Dynamo DB News Items and display on page

                // Retrieve 
                AmazonDynamoDBClient client = new AmazonDynamoDBClient();
                DynamoDBContext context = new DynamoDBContext(client);

                IEnumerable<NewsItem> NewsQueryResults;
                // Scan for all news items not expired                
                string sDate;
                sDate = DateTime.Today.Year.ToString() + DateTime.Today.Month.ToString("00") + DateTime.Today.Day.ToString("00");

                long ltoday = long.Parse(sDate);
                Response.Write("<h2>" + "Corporate News Items as of: " + DateTime.Today.ToLongDateString() + "</h2>" ) ;
                NewsQueryResults = context.Scan<NewsItem>(
                                            new ScanCondition("RemoveDate", ScanOperator.GreaterThanOrEqual, ltoday));

                //foreach (var result in NewsQueryResults)                
                foreach (NewsItem result in NewsQueryResults)
                {
                    // Write news to page                     
                    Response.Write("<br>");
                    Response.Write("<div class=\"row\">");
                    Response.Write("<div class=\"col-md-12\">");                                        
                    Response.Write("<h3>" + result.Title + "</h3>");
                    Response.Write("<p>" + "Date Published: " + result.PublishedDate.ToString() + "</p>");                        
                    Response.Write("<p>" + result.MainContent + "</p>");
                    Response.Write("<p>------------------------------------------------------------------</p>");
                    Response.Write("</div>");
                    Response.Write("</div>");
                }
                ;
            }
            catch (Exception ex)
            {
                Response.Write("An error occured: " + ex.Message);

            }
        }
    }
}