﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Odbc;

namespace SportsWeb
{
    public partial class Customers : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Response.Write("<div class=\"row\">");
                Response.Write("<div class=\"col-md-6\">");
                Response.Write("<h1>Customers</h1>");
                Response.Write("</div>");
                Response.Write("<div class=\"col-md-6\">");
                Response.Write("<img src = \"https://s3.eu-west-2.amazonaws.com/sportswebs3/SportsWebLogo.png\" height=\"100\" width =\"300\" align=\"right\"/>");
                Response.Write("</div>");
                Response.Write("</div>");

                using (OdbcConnection connection = new OdbcConnection(ConfigurationManager.ConnectionStrings["SportsWebConnection"].ConnectionString))
                {
                    connection.Open();
                    using (OdbcCommand command = new OdbcCommand("SELECT * FROM Customers", connection))
                    using (OdbcDataReader dr = command.ExecuteReader())
                    {
                        
                        if (dr.HasRows)
                        {
                            gvCustomers.DataSource = dr;
                            gvCustomers.DataBind();
                        }
                        else
                        {
                            gvCustomers.DataSource = null;
                            gvCustomers.DataBind();
                        }
                    }
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                Response.Write("An error occured: " + ex.Message);
            }
        }
    }
}