﻿<%@ Page Title="Customers" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Customers.aspx.cs" Inherits="SportsWeb.Customers" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h3>Customer List.</h3>    
        <div class="row">
             <div class="col-md-12">
                 <p>
                    <asp:GridView ID="gvCustomers" runat="server" cssClass="gridview"></asp:GridView>
                </p>        
            </div>
        </div>    
</asp:Content>

