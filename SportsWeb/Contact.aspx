﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="SportsWeb.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>Paul Loh</h3>
    <h4>Student No: L022268H</h4>
    <address>
        <strong>Email:</strong> <a href="mailto:L022268H@student.staffs.ac.uk">L022268H@student.staffs.ac.uk</a><br />
    </address>

</asp:Content>
