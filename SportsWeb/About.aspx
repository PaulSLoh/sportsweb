﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="SportsWeb.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>This site is a Proof of Concept to demonstrate a web presence hosted on AWS.</h3>
    <p>This site is the PoC required as part of the work for module Distributed Processing (D/L) - COIS71183</p>
</asp:Content>
